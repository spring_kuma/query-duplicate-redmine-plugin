# This file is a part of Redmin Agile (redmine_agile) plugin,
# Agile board plugin for redmine
#
# Copyright (C) 2011-2014 RedmineCRM
# http://www.redminecrm.com/
#
# redmine_agile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# redmine_agile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with redmine_agile.  If not, see <http://www.gnu.org/licenses/>.

require_dependency 'queries_controller'

module QueryDuplicate
  module Patches

    module QueriesControllerPatch
      def self.included(base)
        base.class_eval do
          alias :old_new :new
          alias  :find_optional_project_original :find_optional_project

          def new
            unless params[:duplicate_from]
              @query = old_new
            else
              @query = IssueQuery.new(@original_query.attributes.dup.except("id", "created_on", "updated_on", "user_id"))
              @query.project = @project
            end
            @query
          end

          def find_optional_project
            if params[:duplicate_from]
              @original_query = IssueQuery.find(params[:duplicate_from], :include => :project)
              @project = @original_query.project
            elsif
              @project = find_optional_project_original
            end
            render_403 unless User.current.allowed_to?(:save_queries, @project, :global => true)
          rescue ActiveRecord::RecordNotFound
            render_404
          end
        end
      end
    end

  end
end

unless QueriesController.included_modules.include?(QueryDuplicate::Patches::QueriesControllerPatch)
  QueriesController.send(:include, QueryDuplicate::Patches::QueriesControllerPatch)
end
