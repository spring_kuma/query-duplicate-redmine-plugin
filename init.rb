# This file is a part of Redmin Agile (redmine_agile) plugin,
# Agile board plugin for redmine
#
# Copyright (C) 2011-2014 RedmineCRM
# http://www.redminecrm.com/
#
# redmine_agile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# redmine_agile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with redmine_agile.  If not, see <http://www.gnu.org/licenses/>.

Redmine::Plugin.register :query_duplicate do
  name 'Query Duplicate'
  author 'svsool'
  description 'Redmine Plugin, which add duplicate button for build-in queries'
  version '0.0.1'
  url 'http://svsool.ru'
  author_url 'svsool@bk.ru'

  requires_redmine :version_or_higher => '2.3'

end

require 'query_duplicate'
