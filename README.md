# Query Duplicate - Redmine Plugin

Redmine Plugin, which add duplicate button for build-in queries

## Install

    $ cd #redmine_directory#/plugins
    $ git clone git@bitbucket.org:svsool/query-duplicate-redmine-plugin.git
    $ #redmine_directory#/script/rails server -b 0.0.0.0 -p 3000 -e production
